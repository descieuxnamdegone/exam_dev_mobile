package com.perso.exam.framgment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.perso.exam.R;
import com.perso.exam.adapter.CityAdapter;
import com.perso.exam.adapter.HousingAdapter;
import com.perso.exam.bo.Logement;
import com.perso.exam.bo.Ville;
import com.perso.exam.viewmodel.ListCityViewModel;
import com.perso.exam.viewmodel.ListHousingViewModel;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the  factory method to
 * create an instance of this fragment.
 */
public class ListHousingFragment extends Fragment {
    OkHttpClient client;
    private static final String TAG = "ListHousingFragment";
    private HousingAdapter adapter;
    private RecyclerView rv;
    private ListHousingViewModel vm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        client = new OkHttpClient();
        vm = new ViewModelProvider(this).get(ListHousingViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_housing_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        initializeHousing();
        Observer<ArrayList<Logement>> observerList = logement -> {
            adapter.setHousingArrayList(logement);
            rv.scrollToPosition(adapter.getItemCount()-1);
        };
        vm.getArrayListHousing().observe(getViewLifecycleOwner(),observerList);
        if(vm.getArrayListHousing().getValue().isEmpty()){
            fetchHousing();
        }

        super.onViewCreated(view, savedInstanceState);
    }

    public void fetchHousing(){
        Log.e(TAG, "onResponse: " + " test" );

        Request requestCity = new Request.Builder()
                .url("https://flutter-learning.mooo.com/logements")
                .build();
        client.newCall(requestCity).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.e(TAG, "onFailure: " + "récupération des logements:" + e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                
                if(response.code() == 200){

                    ArrayList<Logement> allHousing = new Gson().fromJson(
                            response.body().string(),
                            new TypeToken<ArrayList<Logement>>(){}.getType()
                    );
                    vm.getArrayListHousing().postValue(allHousing);


                }else{
                    Log.e(TAG, "onResponse: " + " incorrecte" );
                }
            }
        });
    }

    private void initializeHousing() {

        rv = getView().findViewById(R.id.recyclerViewHousing);
        adapter = new HousingAdapter();
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        rv.setAdapter(adapter);
    }
    }
