package com.perso.exam.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.perso.exam.R;
import com.perso.exam.bo.Logement;
import com.perso.exam.bo.Ville;
import com.perso.exam.databinding.RowLayoutCityBinding;

import java.util.ArrayList;


public class  CityAdapter extends RecyclerView.Adapter<CityHolder> {
    ArrayList<Ville> cityArrayList;
    public CityAdapter() {
       
        this.cityArrayList = new ArrayList<>();
    }
    public void setCityArrayList(ArrayList<Ville> cityArrayList) {
        this.cityArrayList = cityArrayList;
        notifyDataSetChanged();
    }
    @NonNull @Override
    public CityHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowLayoutCityBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.row_layout_city,
                parent,
                false
        );
        return new CityHolder(binding);
    }
    @Override
    public void onBindViewHolder(@NonNull CityHolder holder, int position) {
        //Ville city = cityArrayList.get(position);
        Ville cityToShow = cityArrayList.get(position);
        
        holder.binding.setCity(cityToShow); 

       // holder.textViewName.setText(cityToShow.getName());
    }

    @Override
    public int getItemCount() {
        return cityArrayList.size();
} 
}

