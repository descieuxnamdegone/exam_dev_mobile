package com.perso.exam.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.perso.exam.bo.Ville;

import java.util.ArrayList;


public class  ListCityViewModel extends ViewModel {
    MutableLiveData<ArrayList<Ville>> arrayListCity;

    public MutableLiveData<ArrayList<Ville>> getArrayListCity(){
        if(arrayListCity == null){
            this.arrayListCity = new MutableLiveData<>(new ArrayList<>());
        }
        return this.arrayListCity;
    }

}
