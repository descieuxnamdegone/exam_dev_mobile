package com.perso.exam.bo;

public class Logement {
    int id;
    Ville place;
    String title;
    Float price;
    String created_at;
   // String illustrations;

    public Logement(int id, Ville place, String title, Float price, String created_at) {
        this.id = id;
        this.place = place;
        this.title = title;
        this.price = price;
        this.created_at = created_at;
        //this.illustrations = illustrations;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Ville getPlace() {
        return place;
    }

    public void setPlace(Ville place) {
        this.place = place;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

   }
