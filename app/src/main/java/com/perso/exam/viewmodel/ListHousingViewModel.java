package com.perso.exam.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.perso.exam.bo.Logement;
import com.perso.exam.bo.Ville;

import java.util.ArrayList;


public class ListHousingViewModel extends ViewModel {
    MutableLiveData<ArrayList<Logement>> arrayListHousing;

    public MutableLiveData<ArrayList<Logement>> getArrayListHousing(){
        if(arrayListHousing == null){
            this.arrayListHousing = new MutableLiveData<>(new ArrayList<>());
        }
        return this.arrayListHousing;
    }

}
