package com.perso.exam.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.perso.exam.databinding.RowLayoutCityBinding;


class CityHolder extends RecyclerView.ViewHolder {
    RowLayoutCityBinding binding;

    public CityHolder(@NonNull RowLayoutCityBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
