package com.perso.exam.bo;

public class Ville{
    int id;
    String name;
    String created_at;
    ImageBnb pic;

    public Ville(int id, String name, String created_at) {
        this.id = id;
        this.name = name;
        this.created_at = created_at;
        this.pic = pic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public ImageBnb getPic() {
      return pic;
    }

    public void setPic(ImageBnb pic) {
       this.pic = pic;
    }
}
