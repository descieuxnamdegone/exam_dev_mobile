package com.perso.exam.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.perso.exam.R;
import com.perso.exam.bo.Logement;
import com.perso.exam.bo.Ville;

import com.perso.exam.databinding.RowLayoutHousingBinding;

import java.util.ArrayList;


public class HousingAdapter extends RecyclerView.Adapter<HousingHolder> {
    ArrayList<Logement> housingArrayList;
    public HousingAdapter() {
       
        this.housingArrayList = new ArrayList<>();
    }
    public void setHousingArrayList(ArrayList<Logement> housingArrayList) {
        this.housingArrayList = housingArrayList;
        notifyDataSetChanged();
    }
    @NonNull @Override
    public HousingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowLayoutHousingBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.row_layout_housing,
                parent,
                false
        );
        return new HousingHolder(binding);
    }
    @Override
    public void onBindViewHolder(@NonNull HousingHolder holder, int position) {
       
        Logement housingToShow = housingArrayList.get(position);
        
        holder.binding.setLogement(housingToShow); 

       // holder.textViewName.setText(cityToShow.getName());
    }

    @Override
    public int getItemCount() {
        return housingArrayList.size();
} 
}

