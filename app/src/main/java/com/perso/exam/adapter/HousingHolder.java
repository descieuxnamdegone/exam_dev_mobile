package com.perso.exam.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.perso.exam.databinding.RowLayoutHousingBinding;


class HousingHolder extends RecyclerView.ViewHolder {
    RowLayoutHousingBinding binding;

    public HousingHolder(@NonNull RowLayoutHousingBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
