package com.perso.exam.framgment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.reflect.TypeToken;
import com.perso.exam.R;
import com.perso.exam.adapter.CityAdapter;
import com.perso.exam.bo.Ville;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Response;
import static android.content.Context.MODE_PRIVATE;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;


import com.google.gson.Gson;
import com.perso.exam.viewmodel.ListCityViewModel;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 * Use the  factory method to
 * create an instance of this fragment.
 */
public class ListCityFragment extends Fragment {
    OkHttpClient client;
    private static final String TAG = "ListeCityFragment";
    private CityAdapter adapter;
    private RecyclerView rv;
    private ListCityViewModel vm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        client = new OkHttpClient();
        vm = new ViewModelProvider(this).get(ListCityViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_city, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

       
        initializeCities();
        Observer<ArrayList<Ville>> observerList = ville -> {
            adapter.setCityArrayList(ville);
            rv.scrollToPosition(adapter.getItemCount()-1);
        };
        vm.getArrayListCity().observe(getViewLifecycleOwner(),observerList);
        if(vm.getArrayListCity().getValue().isEmpty()){
            fetchCities();
        }
        
        super.onViewCreated(view, savedInstanceState);
    }

    public void fetchCities(){
        Log.e(TAG, "onResponse: " + " test" );

        Request requestCity = new Request.Builder()
                .url("https://flutter-learning.mooo.com/villes")
                .build();
        client.newCall(requestCity).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.e(TAG, "onFailure: " + "récupération cities:" + e.getMessage());
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if(response.code() == 200){
                    
                    ArrayList<Ville> allCities = new Gson().fromJson(
                            response.body().string(),
                            new TypeToken<ArrayList<Ville>>(){}.getType()
                    );
                     vm.getArrayListCity().postValue(allCities);
                    

                }else{
                    Log.e(TAG, "onResponse: " + " incorrecte" );
                }
            }
        });
    }
   
    private void initializeCities(){
        
        rv = getView().findViewById(R.id.recyclerView);
        adapter = new CityAdapter();
        rv.setLayoutManager(new LinearLayoutManager(getContext()));
        rv.setAdapter(adapter);
    }
}
